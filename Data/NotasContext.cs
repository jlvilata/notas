﻿using Data.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Datos
{
    public class NotasContext : DbContext
    {

        public NotasContext() : base("cnn")
        {
        }

        public DbSet<Nota> Notas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
