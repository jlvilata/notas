﻿using Data.Dto;
using log4net;
using Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Data.Queries
{
    /// <summary>
    /// Clase genérica con las operaciones CRUD básicas
    /// </summary>
    /// <typeparam name="Context">Contexto entre EF y la BDD</typeparam>
    /// <typeparam name="Model">Modelo de datos genérico. Representa un modelo, una tabla de la BDD</typeparam>
    public abstract class QueryBase<Context, Model> : IDisposable where Context : DbContext, new() where Model : class
    {
        #region "Propiedades"

        /// <summary>
        /// Evita llamadas reduntantes
        /// </summary>
        private bool disposedValue = false;
        public readonly Context dbContext;

        public static readonly ILog log = LogManager.GetLogger (MethodBase.GetCurrentMethod().DeclaringType);

        #endregion "Propiedades"

        #region "Constructores"

        public QueryBase() : this(new Context())
        {
        }

        public QueryBase(Context db)
        {
            if (db != null)
                this.dbContext = db;
            else
                this.dbContext = new Context();

            this.dbContext.Configuration.LazyLoadingEnabled = false;
            this.dbContext.Configuration.AutoDetectChangesEnabled = false;
        }

        #endregion "Constructores"

        #region "Métodos y funciones"

        public void Dispose()
        {
            if (!this.disposedValue)
            {
                if (this.dbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                    this.dbContext.Database.Connection.Close();

                this.dbContext.Dispose();
                this.disposedValue = true;
            }
        }

        public async Task<DataResponseDto<bool>> ExistAsync(Model model)
        {
            Model auxM = null;
            DataResponseDto<bool> dataResponseDto = new DataResponseDto<bool>();

            //Obtenemos las primary keys de la entidad
            object[] pkValues = this.ObtenerValoresPK(model);

            var dataResponseDtoAux = await this.ObtenerAsync(pkValues);

            if (dataResponseDtoAux.Ok)
            {
                auxM = dataResponseDtoAux.DatosAdicionales;

                var exist = auxM != null;

                if (exist)
                {
                    // Desvinculamos el modelo del contexto
                    this.dbContext.Entry<Model>(auxM).State = EntityState.Detached;
                    auxM = null;
                }

                dataResponseDto.DatosAdicionales = exist;
            }
            else
                dataResponseDto.Errores = dataResponseDtoAux.Errores;

            return dataResponseDto;
        }

        #endregion "Métodos y funciones"

        #region "Métodos y funciones CRUD"

        /// <summary>
        /// Obtiene todas las entidades del modelo
        /// </summary>
        /// <returns></returns>
        public async Task<DataResponseDto<List<Model>>> ObtenerAsync()
        {
            var respData = new DataResponseDto<List<Model>>();

            try
            {
                respData.DatosAdicionales = await this.dbContext.Set<Model>().ToListAsync(); 
            }
            catch (Exception ex)
            {
                respData.Errores.Add(Mensajes.ErrorInesperado);
                log.Error(ex.ToString());
                throw ex;
            }

            return respData;
        }

        /// <summary>
        /// Obtiene las entidades correspondientes a las primary keys (PK) indicadas
        /// </summary>
        /// <param name="paramsPk">Lista de PK correspondientes a las entidades que queremos recuperar de BDD</param>
        /// <returns></returns>
        public async Task<DataResponseDto<Model>> ObtenerAsync(params object[] paramsPk)
        {
            var respData = new DataResponseDto<Model>();

            try
            {
                if (paramsPk.Length > 0 && !paramsPk.Contains(0))
                    respData.DatosAdicionales = await this.dbContext.Set<Model>().FindAsync(paramsPk);
            }
            catch (Exception ex)
            {
                respData.Errores.Add(Mensajes.ErrorInesperado);
                log.Error(ex.ToString());
            }

            return respData;
        }

        /// <summary>
        /// Obtiene las entidades que cumplen la condición indicada
        /// </summary>
        /// <param name="condicion">Condición que deben cumplir las entidades para ser recuperadas de BDD.</param>
        /// <returns></returns>
        public async Task<DataResponseDto<List<Model>>> ObtenerAsync(Expression<Func<Model, bool>> condicion)
        {
            var respData = new DataResponseDto<List<Model>>();

            try
            {
                respData.DatosAdicionales = 
                    await this.dbContext.Set<Model>().Where(condicion).Select(x => x).ToListAsync();
            }
            catch (Exception ex)
            {
                respData.Errores.Add(Mensajes.ErrorInesperado);
                log.Error(ex.ToString());
                throw ex;
            }

            return respData;
        }

        /// <summary>
        /// Inserta o actualiza una entidad del modelo del contexto proporcionado en el constructor de la clase
        /// </summary>
        /// <param name="model"></param>
        /// <param name="existe">True: Actualiza una entidad; False: Inserta una entidad</param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<DataResponseDto> PersistirAsync(Model model, bool existe, bool capturarExcepciones = true)
        {
            var respData = new DataResponseDto();

            try
            {
                if (existe)
                    //Update
                    this.dbContext.Entry<Model>(model).State = EntityState.Modified;
                else
                    //Insert
                    this.dbContext.Set<Model>().Add(model);

                int filasAfectadas = await this.dbContext.SaveChangesAsync();

                if (filasAfectadas == 0)
                    respData.Errores.Add(Mensajes.EstaOperacionNoAfectoANingunRegistro);
            }
             catch (DbEntityValidationException ex)
            {
                log.Error(ex.ToString());

                if (!capturarExcepciones)
                    throw ex;
            }
            catch (DbUpdateException ex)
            {
                log.Error(ex.ToString());

                if (!capturarExcepciones)
                    throw ex;

                respData.Errores.Add(Mensajes.ErrorInesperado);
            }

            return respData;
        }

        /// <summary>
        /// Elimina una entidad del modelo del contexto proporcionado en el constructor de la clase
        /// </summary>
        /// <param name="id"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<DataResponseDto> EliminarAsync(long id, bool capturarExcepciones = true)
        {
            var model = await this.ObtenerAsync(id);

            return await this.EliminarAsync(model.DatosAdicionales, capturarExcepciones);
        }

        /// <summary>
        /// Elimina una entidad del modelo del contexto proporcionado en el constructor de la clase
        /// </summary>
        /// <param name="model"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<DataResponseDto> EliminarAsync(Model model, bool capturarExcepciones = true)
        {
            var dataResponseDto = new DataResponseDto();

            try
            {
                if (model == null)
                    dataResponseDto.Errores.Add(Mensajes.NoExisteRegistroIndicado);

                this.dbContext.Entry<Model>(model).State = EntityState.Deleted;
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());

                if (!capturarExcepciones)
                    throw ex;

                dataResponseDto.Errores.Add(Mensajes.ErrorInesperado);
            }

            return dataResponseDto;
        }

        #endregion "Métodos y funciones CRUD"

        #region "Métodos y funciones CRUD auxiliares"

        private object[] ObtenerValoresPK(object model)
        {
            var pkNombres = this.ObtenerNombresPKModelo(model);
            var pkValores = this.ObtenerNombresPKModelo(model, pkNombres);

            return pkValores;
        }

        private IEnumerable<string> ObtenerNombresPKModelo(object model)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)this.dbContext).ObjectContext;

            ObjectSet<Model> set = objectContext.CreateObjectSet<Model>();

            IEnumerable<string> pkNombres = set.EntitySet.ElementType
                                            .KeyMembers
                                            .Select(k => k.Name);

            return pkNombres;
        }

        private object[] ObtenerNombresPKModelo(object model, IEnumerable<string> pkNombres)
        {
            int x = 0;
            var valores = new object[pkNombres.Count()];
            var propiedades = model.GetType().GetProperties();

            foreach (var nombre in pkNombres)
            {
                var propiedad = propiedades.Where(w => w.Name == nombre).FirstOrDefault();

                if (propiedad != null)
                    valores[x] = propiedad.GetValue(model, null);
                else
                    valores[x] = null;

                x++;
            }

            return valores;
        }

        #endregion "Métodos y funciones CRUD auxiliares"
    }
}
