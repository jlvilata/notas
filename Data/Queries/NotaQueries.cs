﻿using Data.Dto;
using Data.Models;
using Datos;
using Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Queries
{
    public class NotaQueries : QueryBase<NotasContext, Nota>
    {

        #region "Filtros"

        /// <summary>
        /// Filtra las notas en base a unos valores de ciertos campos
        /// </summary>
        /// <param name="titulo"></param>
        /// <param name="texto"></param>
        /// <param name="fechaDesde"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="anclada"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<DataResponseDto<List<Nota>>> FiltrarAsync(string titulo, string texto, 
            DateTime? fechaDesde, DateTime? fechaHasta, bool? anclada, bool capturarExcepciones = true)
        {
            var respData = new DataResponseDto<List<Nota>>();

            try
            {
                var filtroTitulo = titulo != null && titulo.Trim().Count() > 0;
                var filtroTexto = texto != null && texto.Trim().Count() > 0;
                var filtroFecha = fechaDesde != null && fechaHasta != null;
                var filtroAnclada = anclada != null;

                var query = from n in this.dbContext.Notas
                            where n.Activo
                            && (filtroAnclada ? n.Anclada == anclada : 1 == 1)
                            && ((filtroTitulo ? n.Titulo.Contains(titulo) : 1 ==1)
                            || (filtroTexto ? n.Texto.Contains(texto) : 1 == 1))
                            && (filtroFecha ? (n.FechaCreacion >= fechaDesde && n.FechaCreacion <= fechaHasta) : 1 == 1)
                            select n;

                var notaList = await (query).ToListAsync();

                respData.DatosAdicionales = notaList;
            }
            catch (Exception ex)
            {
                respData.Errores.Add(Mensajes.ErrorInesperado);
                log.Error(ex.ToString());

                if (!capturarExcepciones)
                    throw ex;
            }

            return respData;
        }

        #endregion "Filtros"
    }
}
