﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class Nota
    {
        [Key]
        public int NotaId { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public DateTime? FechaEliminacion { get; set; }
        public bool Activo { get; set; }
        public bool Anclada { get; set; }
        public string Color { get; set; }
    }
}
