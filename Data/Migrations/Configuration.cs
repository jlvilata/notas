namespace Data.Migrations
{
    using Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Datos.NotasContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Datos.NotasContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            //var notas = new List<Nota>
            //{

            //    new Nota {  Titulo = "Lista compra", Texto = "<p>Pan, arroz, verduras, agua</p>",
            //                FechaCreacion = new DateTime(2019,08,08), FechaModificacion = new DateTime(2019,08,08),
            //                Activo = false, Anclada = false, FechaEliminacion = new DateTime(2019,08,09), Color = "F7F779"},

            //    new Nota {  Titulo = "Veterinario", Texto = "<p>Llevar a Canela al veterinario. Viernes 10:30</p>",
            //                FechaCreacion = new DateTime(2019,08,10), FechaModificacion = new DateTime(2019,08,10),
            //                Activo = true, Anclada = true, Color = "F7F779" },

            //    new Nota {  Titulo = "Prueba Preference", Texto = "<p>Hacer la prueba t�cnica para Preference</p>",
            //                FechaCreacion = new DateTime(2019,08,09), FechaModificacion = new DateTime(2019,08,09),
            //                Activo = true, Anclada = false, Color = "F7F779"},

            //    new Nota {  Titulo = "Recoger a Nadia", Texto = "<p>Recoger a Nadia en el aeropuerto. Lunes 19:30</p>",
            //                FechaCreacion = new DateTime(2019,08,15), FechaModificacion = new DateTime(2019,08,15),
            //                Activo = true, Anclada = true, Color = "F7F779"}
            //};

            //notas.ForEach(x => context.Notas.Add(x));
            //context.SaveChanges();
        }
    }
}
