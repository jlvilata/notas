namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class crear_modelo_datos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Nota",
                c => new
                    {
                        NotaId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(),
                        Texto = c.String(),
                        FechaCreacion = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        FechaEliminacion = c.DateTime(),
                        Activo = c.Boolean(nullable: false),
                        Anclada = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.NotaId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Nota");
        }
    }
}
