namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class añadir_columna_color_en_notas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nota", "Color", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nota", "Color");
        }
    }
}
