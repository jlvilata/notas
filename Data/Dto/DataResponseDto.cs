﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Dto
{
    public class DataResponseDto
    {

        #region "Propiedades"

        public List<string> Errores { get; set; }

        public bool Ok
        {
            get
            {
                return !this.Errores.Any();
            }
        }

        #endregion "Propiedades"

        #region "Constructores"

        public DataResponseDto()
        {
            this.Errores = new List<string>();
        }

        #endregion "Constructores"

    }

    public class DataResponseDto<T> : DataResponseDto
    {
        public T DatosAdicionales { get; set; }
    }
}
