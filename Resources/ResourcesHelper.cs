﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace Resources
{
    public class ResourcesHelper
    {
        public dynamic ObtenerRecursos()
        {
            var lista = new ExpandoObject() as IDictionary<string, object>;

            //Etiquetas.resx
            ObtenerPropiedadesRecurso(typeof(Etiquetas), lista);
            //Mensajes.resx
            ObtenerPropiedadesRecurso(typeof(Mensajes), lista);

            return lista;
        }

        private void ObtenerPropiedadesRecurso (Type tipoRecurso, IDictionary<string, object> lista)
        {
            foreach (var m in tipoRecurso
               .GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)
               .Where(p => p.PropertyType.Name != "ResourceManager"))
            {
                lista[m.Name] = m.GetValue(null);
            }
        }
    }
}
