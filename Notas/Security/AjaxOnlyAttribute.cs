﻿using Resources;
using System;
using System.Web.Mvc;

namespace Notas.Security
{
    public class AjaxOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
                throw new Exception(Mensajes.IntentoAccesoDistintoDeAjax);
        }
    }
}