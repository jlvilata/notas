﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Notas.Controllers.Error
{
    public class ErrorController : Controller
    {
        // GET: Error        
        public ActionResult Code(int id)
        {
            var errores = new List<int> { 401, 404, 500 };

            if (!errores.Contains(id))
                id = 404;

            return View(id.ToString());
        }
    }
}