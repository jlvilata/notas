﻿using Business.Dto;
using Business.Domain;
using Notas.Security;
using System.Threading.Tasks;
using System.Web.Mvc;
using Resources;
using System.Collections.Generic;
using System;

namespace Notas.Controllers
{
    public class NotaController : Controller
    {
        
        public async Task<ActionResult> Index(string tituloyTexto_filtro, string fecha_desde_filtro, string fecha_hasta_filtro, string anclada_filtro)
        {
            var respBusiness = new BusinessResponseDto<List<NotaDto>>();

            ViewBag.Title = Etiquetas.Notas;

            FiltroNotaDto filtroNota = new FiltroNotaDto {
                Texto = tituloyTexto_filtro,
                Titulo = tituloyTexto_filtro,
                FechaDesde = (fecha_desde_filtro != null && fecha_desde_filtro.Trim().Length > 0) ? Convert.ToDateTime(fecha_desde_filtro): new DateTime?(),
                FechaHasta = (fecha_hasta_filtro != null && fecha_hasta_filtro.Trim().Length > 0) ? Convert.ToDateTime(fecha_hasta_filtro) : new DateTime?(),
                Anclada = (anclada_filtro != null && anclada_filtro.Trim().Length > 0) ? (anclada_filtro.ToLower() == "on" ? true : false) : new bool?()
            };

            if (filtroNota.Existe)
            {
                respBusiness = await new NotaDomain().FiltrarAsync(filtroNota);

                if (respBusiness.Ok)
                    ViewBag.NumeroResultadosBusqueda = respBusiness.DatosAdicionales.Count;
                else
                    ViewBag.NumeroResultadosBusqueda = 0;
            }
            else
                respBusiness = await new NotaDomain().ObtenerActivasAsync();

            return View(respBusiness.DatosAdicionales);
        }

        // GET: Nota/Details/5
        [AjaxOnly, HttpPost]
        public async Task<JsonResult> ObtenerAsync(int id)
        {
            var respBusiness = await new NotaDomain().ObtenerAsync(id);
          
            return Json(respBusiness);
        }

        [AjaxOnly, HttpPost]
        public async Task<JsonResult> ObtenerListadoPapeleraAsync()
        {
            var resp = await new NotaDomain().ObtenerPapeleraAsync();

            return Json(resp);
        }

        /// <summary>
        /// Crea una nota
        /// </summary>
        /// <returns></returns>
        [AjaxOnly, HttpPost]
        public async Task<JsonResult> CrearAsync()
        {
            var respBusiness = await new NotaDomain().CrearAsync(Request);
            return Json(respBusiness);
        }

        [AjaxOnly, HttpPost]
        public async Task<JsonResult> ModificarAsync()
        {
            var respBusiness = await new NotaDomain().ModificarAsync(Request);
            return Json(respBusiness);
        }

        /// <summary>
        /// Desactiva una nota. La borra pero no la elimina de BDD, sino que la envía a la papelera.
        /// </summary>
        /// <param name="notaId"></param>
        /// <returns></returns>
        [AjaxOnly, HttpPost]
        public async Task<JsonResult> ReciclarAsync(int notaId)
        {
            var resp = new BusinessResponseDto();

            if (notaId != 0)
                resp = await new NotaDomain().ReciclarAsync(notaId);

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Recupera una nota. La activa, ya que estaba desactivada (eliminada).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly, HttpPost]
        public async Task<JsonResult> RestaurarAsync(int id)
        {
            var respBusiness = await new NotaDomain().RecuperarAsync(id, true);
            return Json(respBusiness);
        }

        /// <summary>
        /// Vacía la papelera de reciclaje. Elimina de BDD las notas con active = false
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly, HttpPost]
        public async Task<JsonResult> VaciaPapeleraAsync()
        {
            var respBusiness = await new NotaDomain().VaciaPapeleraAsync();
            return Json(respBusiness);
        }
    }
}
