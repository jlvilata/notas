﻿using Newtonsoft.Json;
using Notas.Security;
using Resources;
using System.Web.Mvc;
using System.Web.UI;

namespace Notas.Controllers
{
    public class AppController: Controller
    {

        [AllowAnonymous, AjaxOnly, OutputCache(Duration = 86400, Location = OutputCacheLocation.Server)] //1 día
        public JsonResult ObtenerDatosLocalesNotas()
        {
            var res = new { Traducciones = new ResourcesHelper().ObtenerRecursos() };

            string result = JsonConvert.SerializeObject(res, new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}