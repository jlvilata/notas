﻿if (Notas.Papelera === undefined)
    Notas.Papelera = {};


//Obtenemos el listado de las notas NO activas en el sistema
Notas.Papelera.ObtenerListadoPapeleraAsync = function () {

    $('#ListadoNotas').DataTable(
        {
            language: {
                url: "/Scripts/Plugins/DataTables/culture/es-ES.json"
            },
            //order: [[0, "desc"]],
            paging: false,
            ordering: false,
            info: false,
            dom: 'Brtip',
            buttons: [],
            processing: true,
            serverSide: true,
            ajax: {
                url: '/Nota/ObtenerListadoPapeleraAsync',
                type: "POST"
            },
            columns: [
                {
                    data: 'NotaId',
                    visible: false
                },
                { data: 'Titulo' },
                { data: 'Texto' },
                {
                    data: 'FechaCreacion',
                    render: function (data, type, row) {
                        return moment(data).format("DD-MM-YYYY");
                    }
                }
                //{
                //    data: 'acciones',
                //    searchable: false,
                //    orderable: false,
                //    render: function (data, type, row, meta) {
                //        var enlaces = Notas.Nota.PintaBotonesAccionesListaNotas(row, meta.row);
                //        return enlaces;
                //    }
                //}
            ],
            responsive: true
        }
    );
};




