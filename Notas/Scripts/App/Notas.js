﻿Notas = {};
Notas.cultura = null;
Notas.traducciones = {};

//Comprueba si el navegador soporta HTML5 local storage
Notas.soportalocalStorage = function () {
    return 'localStorage' in window && window['localStorage'] !== null;
};

//Permite usar, en el lado del cliente, los resources de multiidioma
Notas.ObtenerTraducciones = function () {
    if (Notas.soportalocalStorage) {
        var data = localStorage.getItem('ObtenerDatosLocalesNotas');

        if (data != null) {
            var res = JSON.parse(data);
            Notas.traducciones = res.Traducciones;

            Notas.ObtenerDatosLocalesNotas(true);
        }
        else {
            Notas.ObtenerDatosLocalesNotas(false);
        }
    }
    else {
        Notas.ObtenerDatosLocalesNotas(false);
    }
};

//Obtiene del servidor los resources de multiidioma para usarlos en el lado del cliente
Notas.ObtenerDatosLocalesNotas = function (is_async) {
    $.ajax({
        url: '/App/ObtenerDatosLocalesNotas',
        dataType: 'json',
        async: is_async,
        success: function (data) {
            if (Notas.soportalocalStorage) {
                localStorage.setItem('ObtenerDatosLocalesNotas', data);
            }

            var res = JSON.parse(data);
            Notas.traducciones = res.Traducciones;
        }
    });
};

//Inicializa los objetos datePicker
Notas.LoadDatePicker = function (fromDateId, toDateId) {
    var from = $("#" + fromDateId)
        .datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true
        })
        .on("change", function () {
            to.datepicker("option", "minDate", getDate(this));
        }),
        to = $("#" + toDateId).datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true
        })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        var dateFormat = "dd-mm-yy";
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }
};

//Valida los inputs de un formulario determinado, conforme a unas reglas definidas
Notas.ValidaFormulario = function (form) {
    var formulario = $('#' + form);

    formulario.validate({
        rules: { //Hay que usar el valor del atributo 'Name' del elemento a validar para identificarlo
            'Titulo': {
                required: true
            }
        },
        messages: {
            'Titulo': {
                required: Notas.traducciones.ElCampoEsObligatorio
            }
        }, //Define los estilos que utiliza el validador de inputs
        errorClass: "validate-error",
        highlight: function (element) {
                $(element).parent('div').addClass("has-error");
        },
        unhighlight: function (element) {
                $(element).parent('div').removeClass("has-error");
        }
    });

    var validacionOK = formulario.valid(); //Validamos antes para que renderice los mensajes de error, si los hay.
    
    return validacionOK;
};

//Oculta los posibles mensajes de error de una validación previa
Notas.FormLimpiarErroresDeValidacion = function () {
    $('.has-error').removeClass('has-error');
    $('.validation-error').remove();
    $('label.validate-error').remove();
    $('.validate-error').removeClass('validate-error');
};

//Crea un objeto FormData con los datos existentes dentro del selector proporcionado
Notas.ObtenerFormDataPantalla = function (selector) {
    var objetoDatos = Notas.ObtenerInformacionPantalla(selector);

    var data = new FormData();

    jQuery.each(objetoDatos, function (prop, valor) {
        data.append(prop, valor);
    });

    return data;
};

//Crea un objeto JSON con los datos existentes dentro del selector proporcionado
Notas.ObtenerInformacionPantalla = function (selector) {
    var objetoDatos = {};
    var inputs = $(selector).find('input, textarea, select');
    var top = inputs.length;

    for (var index = 0; index < top; index++) {
        var input = inputs[index];
        var tieneNombre = !!input.name;
        var esRadio = $(input).is('[type=radio]');
        var esCheck = $(input).is('[type=checkbox]');

        if (tieneNombre && (!esRadio || esCheck)) {
            var valor = undefined;

            if (esCheck) {
                valor = $(input).is(":checked")
            }
            else {
                valor = $(input).val();
            }

            objetoDatos[input.name] = valor;
        }
    }

    return objetoDatos;
};

//Función que reemplaza argumentos por valores. Equivale a String.Format de c#. Ejemplo de llamada: Notas.StringFormat('cadena que quiero {0} con {1}', 'reemplazar', 'valores')
Notas.StringFormat = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}