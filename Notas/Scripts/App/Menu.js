﻿if (Notas.Menu === undefined)
    Notas.Menu = {};

var quill = '';

//Inicializa todos los eventos y parámetros necesarios para el correcto funcionamiento de la página
Notas.Menu.InicializarEventos = function () {

    Notas.Menu.ActivarEditorTextoEnriquecido('#formModalNota #editorTextoEnriquecido');

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('#modalNota #color_nueva_modal').on('change', function () {
        $("#modalNota #titulo_nueva_nota").css('background-color', '#' + this.jscolor);
        $("#modalNota #editorTextoEnriquecido").css('background-color', '#' + this.jscolor);
    });
    
    $("#botonSubmitNota").click(function (e) {
        e.preventDefault();

        //Guardamos el texto enriquecido que está en un div, en un textarea para pdoer pasarlo al 
        //lado del servidor y persistirlo
        var inputTextoOculto = document.querySelector('input[name=__Texto]');
        inputTextoOculto.value = quill.container.firstChild.innerHTML;

        var crearNuevo = $("#modalNota #NotaId").val() === "0";

        if (Notas.ValidaFormulario('formModalNota')) {
            if (crearNuevo) {
                Notas.Menu.CrearNota();
            }
            else {
                Notas.Menu.ModificarNota();
            }
        }
    });
};

//Activa todos los editores WYSIWYG del formulario especificado
Notas.Menu.ActivarEditorTextoEnriquecido = function (input) {
    quill = new Quill(input, {
        modules: {
            toolbar: [
                ['bold', 'italic', 'underline'],
                [{ 'color': [] }, { 'background': [] }],   
                [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                ['clean']  
            ] },
        theme: 'snow'
    });
};

Notas.Menu.InicializaModalNota = function () {
    Notas.FormLimpiarErroresDeValidacion();
    
    $("#modalNota #tituloModalNota").text(Notas.StringFormat(Notas.traducciones.NuevaX, Notas.traducciones.Nota));
    $("#modalNota #NotaId").val('0');
    $("#modalNota #titulo_nueva_nota").val('');

    $("#modalNota #editorTextoEnriquecido")[0].firstChild.innerHTML = "<p><br></p>";

    $("#modalNota #anclada_nueva_nota").prop('checked', false);
    $("#modalNota #color_nueva_modal").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());
    $("#modalNota #titulo_nueva_nota").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());
    $("#modalNota #editorTextoEnriquecido").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());

    $("#modalNota #botonSubmitNota").text(Notas.traducciones.Crear);
};

Notas.Menu.CrearNota = function () {
    var formData = Notas.ObtenerFormDataPantalla("#formModalNota");//new FormData(); 

    $.ajax(
        {
            type: "POST",
            url: '/Nota/CrearAsync',
            data: formData,
            processData: false,
            contentType: false
        }
    ).done(
        function (resp) {
            if (resp.Ok) {
                Swal.fire({
                    type: "success",
                    title: Notas.traducciones.Confirmacion,
                    text: Notas.traducciones.NotaGuardada,
                    preConfirm: function () {
                            window.location.href = "/Nota/Index";
                    }
                });
            }
            else {
                Swal.fire({
                    type: "error",
                    title: Notas.traducciones.Error,
                    text: resp.Errores
                });
            }
        });
};

Notas.Menu.ModificarNota = function () {
    var formData = Notas.ObtenerFormDataPantalla("#formModalNota");//new FormData(); 

    $.ajax(
        {
            type: "POST",
            url: '/Nota/ModificarAsync',
            data: formData,
            processData: false,
            contentType: false
        }
    ).done(
        function (resp) {
            if (resp.Ok) {
                Swal.fire({
                    type: "success",
                    title: Notas.traducciones.Confirmacion,
                    text: Notas.traducciones.CambiosGuardados,
                    preConfirm: function () {
                        window.location.href = "/Nota/Index";
                    }
                });
            }
            else {
                Swal.fire({
                    type: "error",
                    title: Notas.traducciones.Error,
                    text: resp.Errores
                });
            }
        });
};

