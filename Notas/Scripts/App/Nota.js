﻿if (Notas.Nota === undefined)
    Notas.Nota = {};

//Inicializa todos los eventos y parámetros necesarios para el correcto funcionamiento de la página
Notas.Nota.InicializarEventos = function () {

    $("[id*='headerIndividualNota']").mouseover(function () {
        Notas.Nota.MostarOcultarMenuNota(this, true);
    });

    $("[id*='headerIndividualNota']").mouseout(function () {
        Notas.Nota.MostarOcultarMenuNota(this, false);
    });

    Notas.LoadDatePicker("fecha_desde_filtro", "fecha_hasta_filtro");
    Notas.ObtenerTraducciones();
};

Notas.Nota.MostarOcultarMenuNota = function (nota, mostrar) {
    var posInicial = nota.id.indexOf("_");
    var notaId = nota.id.substring(posInicial + 1, nota.id.length);

    if (mostrar) {
        $("#menuIndividualNota_" + notaId).removeClass("hidden");
    }
    else {
        $("#menuIndividualNota_" + notaId).addClass("hidden");
    }
};

//Enviamos a la papelera de reciclaje una nota
Notas.Nota.ReciclarNota = function (notaId, titulo) {

    Swal.fire({
        title: Notas.traducciones.Confirmacion,
        text: Notas.StringFormat(Notas.traducciones.SeguroEliminarNotaX, titulo),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#007bff',
        cancelButtonColor: '#d33',
        confirmButtonText: Notas.traducciones.Eliminar,
        cancelButtonText: Notas.traducciones.Cancelar
    }).then((result) => {
        if (result.value) {
            $.ajax(
                {
                    type: "POST",
                    url: '/Nota/ReciclarAsync',
                    data: {
                        notaId: notaId
                    },
                    dataType: "json"
                }
            ).done(
                function (resp) {
                    if (resp.Ok) {
                        window.location.href = "/Nota/Index";
                    }
                    else {
                        Swal.fire({
                            type: "error",
                            title: Notas.traducciones.Error,
                            text: resp.Errores
                        });
                    }
                });
        }
    })
};

//Recuperamos la info de una nota para editarla
Notas.Nota.EditarNota = function (model) {
    Notas.FormLimpiarErroresDeValidacion();

    $("#modalNota #tituloModalNota").text(Notas.StringFormat(Notas.traducciones.EditarX, Notas.traducciones.Nota));
    $("#modalNota #NotaId").val(model.NotaId);
    $("#modalNota #Activo").val(model.Activo);
    $("#modalNota #FechaCreacion").val(moment(model.FechaCreacion).format("DD-MM-YYYY"));
    $("#modalNota #titulo_nueva_nota").val(model.Titulo);
    
    $("#modalNota #editorTextoEnriquecido")[0].firstChild.innerHTML = model.Texto;

    $("#modalNota #anclada_nueva_nota").prop('checked', model.Anclada);
    $("#modalNota #color_nueva_modal").val(model.Color);
    $("#modalNota #color_nueva_modal").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());
    $("#modalNota #titulo_nueva_nota").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());
    $("#modalNota #editorTextoEnriquecido").css('background-color', '#' + $("#modalNota #color_nueva_modal").val());

    $("#modalNota #botonSubmitNota").text(Notas.traducciones.Guardar);
    
    $('#modalNota').modal('show');
};