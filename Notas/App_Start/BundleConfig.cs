﻿using System.Web.Optimization;

namespace Notas
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region "JS's propios de la app"

            var appJS = new ScriptBundle("~/bundles/app_JS").Include(
                "~/Scripts/App/Notas.js",
                "~/Scripts/App/Nota.js",
                "~/Scripts/App/Menu.js",
                "~/Scripts/App/Papelera.js");
            
            bundles.Add(appJS);

            #endregion "JS's propios de la app"

            #region "JS's genéricos"

            var mainJS = new ScriptBundle("~/bundles/main_JS").Include(
               "~/Scripts/jquery-{version}.js",
               "~/Scripts/jquery.validate.min.js",
               "~/Scripts/jquery-ui.min.js");

            bundles.Add(mainJS);

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            #endregion "JS's genéricos"

            #region "Estilos"

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/bootstrap.custom.css",
                     "~/Content/site.css",
                     "~/Content/dataTables.bootstrap.min.css",
                     "~/Content/responsive.dataTables.min.css",
                     "~/Content/dataTables.fontAwesome.css",
                     "~/Content/responsive.bootstrap.min.css",
                     "~/Content/buttons.dataTables.min",
                     "~/Content/datatables.min.css",
                     "~/Content/bootstrap.colorpickersliders.min.css",
                     "~/Content/quill.snow.css",
                     "~/Content/searchBar.css"));


            #endregion "Estilos"

            #region "JS's externos"

            var pluginsJS = new ScriptBundle("~/bundles/plugins_JS").Include(
                "~/Scripts/Plugins/DataTables/datatables.min.js",
                "~/Scripts/Plugins/DataTables/dataTables.bootstrap.min.js",
                "~/Scripts/Plugins/DataTables/dataTables.responsive.min.js",
                "~/Scripts/Plugins/DataTables/jquery.dataTables.min.js",
                "~/Scripts/Plugins/DataTables/dataTables.buttons.js",
                "~/Scripts/Plugins/DateTimePicker/moment.js",
                "~/Scripts/Plugins/DateTimePicker/datetime-moment.js",
                "~/Scripts/Plugins/DateTimePicker/datetime.js",
                "~/Scripts/Plugins/sweetAlert2.min.js",
                "~/Scripts/Plugins/ColorPicker/tinycolor.js",
                "~/Scripts/Plugins/ColorPicker/bootstrap.colorpickersliders.min.js",
                "~/Scripts/Plugins/ColorPicker/jscolor.js",
                "~/Scripts/Plugins/quill.min.js");
            
            bundles.Add(pluginsJS);

            #endregion "JS's externos"
        }
    }
}
