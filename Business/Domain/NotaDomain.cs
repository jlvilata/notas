﻿using Data.Queries;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Dto;
using Data.Dto;
using System.Linq.Expressions;
using System.Linq;
using Resources;
using System.Web;
using System.Collections.Specialized;
using log4net;
using System.Reflection;

namespace Business.Domain
{
    public class NotaDomain
    {
        #region "Propiedades"

        public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion "Propiedades"

        #region "Listar"

        public async Task<BusinessResponseDto<NotaDto>> ObtenerAsync(int id)
        {
            using (var nq = new NotaQueries())
            {
                var respDatos = new DataResponseDto<Nota>();
                var respBusiness = new BusinessResponseDto<NotaDto>();

                respDatos =  await nq.ObtenerAsync(id);

                if (respDatos.Ok)
                    respBusiness.DatosAdicionales = new NotaDto(respDatos.DatosAdicionales);
                else
                    respBusiness.Errores = respDatos.Errores;

                return respBusiness;
            }
        }

        /// <summary>
        /// Obtiene solamente las notas activas
        /// </summary>
        /// <returns></returns>
        public async Task<BusinessResponseDto<List<NotaDto>>> ObtenerActivasAsync()
        {
            return await ObtenerAsync(n => n.Activo);
        }

        /// <summary>
        /// Obtiene las notas que se han borrado (desactivado) y están en la papelera
        /// </summary>
        /// <returns></returns>
        public async Task<GridResponseDto<NotaDto>> ObtenerPapeleraAsync()
        {
            var resp = new GridResponseDto<NotaDto>();

            var respBusinessResponseDto = await ObtenerAsync(n => !n.Activo);

            if (respBusinessResponseDto.Ok)
                resp.data = respBusinessResponseDto.DatosAdicionales;

            return resp;
        }
        
        private async Task<BusinessResponseDto<List<NotaDto>>> ObtenerAsync(Expression<Func<Nota, bool>> condicion)
        {
            using (var nq = new NotaQueries())
            {
                var respDatos = new DataResponseDto<List<Nota>>();
                var respBusiness = new BusinessResponseDto<List<NotaDto>>();

                respDatos = await nq.ObtenerAsync(condicion);

                if (respDatos.Ok)
                {
                    var notaDtoList = NotaDto.Nota_to_NotaDto(respDatos.DatosAdicionales);

                    respBusiness.DatosAdicionales = new List<NotaDto>();
                    respBusiness.DatosAdicionales.AddRange(notaDtoList);
                }
                else
                    respBusiness.Errores = respDatos.Errores;

                return respBusiness;
            }
        }

        ///// <summary>
        ///// Obtiene las notas (filtradas o no), preparadas para ser paginadas en un grid (datatable)
        ///// </summary>
        ///// <param name="gridRequest"></param>
        ///// <param name="filtroNota"></param>
        ///// <returns></returns>
        //public async Task<BusinessResponseDto<List<NotaDto>>> ObtenerAsync(FiltroNotaDto filtroNota)
        //{
        //    if (filtroNota.Existe)
        //        return (await FiltrarAsync(filtroNota));
        //    else
        //        return (await ObtenerActivasAsync());
        //}

        ///// <summary>
        ///// Obtiene las notas (filtradas o no), preparadas para ser paginadas en un grid (datatable)
        ///// </summary>
        ///// <param name="gridRequest"></param>
        ///// <param name="filtroNota"></param>
        ///// <returns></returns>
        //public async Task<GridResponseDto<NotaDto>> ObtenerAsync(GridRequestDto gridRequest, FiltroNotaDto filtroNota)
        //{
        //    var resp = new GridResponseDto<NotaDto>();

        //    if (filtroNota.Existe)
        //        resp.data = (await FiltrarAsync(filtroNota)).DatosAdicionales;
        //    else
        //        resp.data = (await ObtenerActivasAsync()).DatosAdicionales;

        //    return resp;
        //}

        #endregion "Listar"

        #region "Persistir"

        /// <summary>
        /// Crea una nueva nota en la BDD
        /// </summary>
        /// <param name="request"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto> CrearAsync(HttpRequestBase request, bool capturarExcepciones = true)
        {
            var respBusiness = new BusinessResponseDto();

            var notaDto = ObtenerNotaRequest(request, respBusiness);

            if (respBusiness.Ok)
                respBusiness = await PersistirAsync(notaDto, false, capturarExcepciones);

            return respBusiness;
        }

        /// <summary>
        /// Modifica una nota de la BDD
        /// </summary>
        /// <param name="request"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto> ModificarAsync(HttpRequestBase request, bool capturarExcepciones = true)
        {
            var respBusiness = new BusinessResponseDto();

            var notaDto = ObtenerNotaRequest(request, respBusiness);

            if (respBusiness.Ok)
                respBusiness = await PersistirAsync(notaDto, true, capturarExcepciones);

            return respBusiness;
        }

        /// <summary>
        /// Inserta o actualiza una nota en la BDD 
        /// </summary>
        /// <param name="notaDto"></param>
        /// <param name="existe">True: Actualiza la nota; False: Inserta una nota</param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        private async Task<BusinessResponseDto> PersistirAsync(NotaDto notaDto, bool existe, bool capturarExcepciones = true)
        {
            using (var nq = new NotaQueries())
            {
                var respDatos = new DataResponseDto();
                var respBusiness = new BusinessResponseDto();

                var nota =  Utils.Conversors.NotaDto_to_Nota(notaDto);

                nota.FechaModificacion = DateTime.Now;

                if (!existe) //Solo si es nueva
                {
                    nota.FechaCreacion = DateTime.Now;
                    nota.Activo = true;
                }

                respDatos = await nq.PersistirAsync(nota, existe, capturarExcepciones);

                respBusiness.Errores = respDatos.Errores;

                return respBusiness;
            }
        }

        #endregion "Persistir"

        #region "Activar y desactivar"

        /// <summary>
        /// Activa o desactiva una nota. Si desactiva, la borra pero no la elimina de BDD, sino que la envía a la papelera.
        /// </summary>
        /// <param name="notaId"></param>
        /// <param name="activar">True: Activa la nota; False: Desactiva la nota</param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        private async Task<BusinessResponseDto> ActiveToggleAsync(int notaId, bool activar, bool capturarExcepciones = true)
        {
            var resp = new BusinessResponseDto();

            var businessResponseDto = await ObtenerAsync(notaId);

            if (businessResponseDto.Ok)
            {
                var notaDto = businessResponseDto.DatosAdicionales;

                notaDto.Activo = activar;

                if (!activar) //Si estamos reciclando
                    notaDto.FechaEliminacion = DateTime.Now;

                resp = await PersistirAsync(notaDto, true, capturarExcepciones);
            }

            return resp;
        }

        #endregion "Activar y desactivar"

        #region "Emininar (Vacíar papelera)"

        /// <summary>
        /// Elimina una entidad del modelo del contexto proporcionado en el constructor de la clase
        /// </summary>
        /// <param name="notaId"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto> VaciaPapeleraAsync(bool capturarExcepciones = true)
        {
            using (var nq = new NotaQueries())
            {
                var respDatos = new DataResponseDto();
                var respBusiness = new BusinessResponseDto();

                //1º.- Buscamos todas las notas inactivas
                var businessResponseDto = await ObtenerAsync(n => n.Activo == false);

                //2º.- Eliminamos cada una de las notas inactivas
                if (businessResponseDto.Ok)
                {
                    var notaInactiveList = Utils.Conversors.NotaDto_to_Nota(businessResponseDto.DatosAdicionales);

                    foreach (Nota n in notaInactiveList)
                    {
                        respDatos = await nq.EliminarAsync(n);
                        respBusiness.Errores.AddRange(respDatos.Errores);
                    }
                }
                else
                    respBusiness.Errores = businessResponseDto.Errores;

                return respBusiness;
            }
        }

        #endregion "Emininar (Vacíar papelera)"

        #region "Papelera"

        /// <summary>
        /// Envía una nota determinada a la papelera de reciclaje
        /// </summary>
        /// <param name="notaId"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto> ReciclarAsync(int notaId, bool capturarExcepciones = true)
        {
            return await ActiveToggleAsync(notaId, false, capturarExcepciones);
        }

        /// <summary>
        /// Recupera una nota determinada de la papelera de reciclaje
        /// </summary>
        /// <param name="notaId"></param>
        /// <param name="capturarExcepciones"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto> RecuperarAsync(int notaId, bool capturarExcepciones = true)
        {
            return await ActiveToggleAsync(notaId, true, capturarExcepciones);
        }

        #endregion "Papelera"

        //#region "Anclar y desanclar"

        ///// <summary>
        ///// Ancla o no, una nota. Si la ancla aparecerá la primera del listado.
        ///// </summary>
        ///// <param name="notaId"></param>
        ///// <param name="anclar">True: Ancla la nota y aparece la primera al listarse; False: Desancla una nota</param>
        ///// <param name="capturarExcepciones"></param>
        ///// <returns></returns>
        //public async Task<BusinessResponseDto> AnchorToggleAsync(int notaId, bool anclar, bool capturarExcepciones = true)
        //{
        //    var resp = new BusinessResponseDto();

        //    var businessResponseDto = await ObtenerAsync(notaId);

        //    if (businessResponseDto.Ok)
        //    {
        //        var notaDto = businessResponseDto.DatosAdicionales;

        //        notaDto.Anclada = anclar;

        //        resp = await PersistirAsync(notaDto, true, capturarExcepciones);
        //    }

        //    return resp;
        //}

        //#endregion "Anclar y desanclar"

        #region "Filtrar

        /// <summary>
        /// Filtra las notas en base a unos valores de ciertos campos
        /// </summary>
        /// <param name="filtroNotaDto"></param>
        /// <returns></returns>
        public async Task<BusinessResponseDto<List<NotaDto>>> FiltrarAsync(FiltroNotaDto filtroNotaDto)
        {
            var respBusiness = new BusinessResponseDto<List<NotaDto>>();

            if (filtroNotaDto.Existe)
            {
                var respDatos = new DataResponseDto<List<Nota>>();

                using (var nq = new NotaQueries())
                {
                    respDatos = await nq.FiltrarAsync(filtroNotaDto.Titulo, filtroNotaDto.Texto,
                        filtroNotaDto.FechaDesde, filtroNotaDto.FechaHasta, filtroNotaDto.Anclada);

                    if (respDatos.Ok)
                    {
                        var notasList = respDatos.DatosAdicionales;

                        var notaDtoList = NotaDto.Nota_to_NotaDto(notasList);

                        respBusiness.DatosAdicionales = new List<NotaDto>();
                        respBusiness.DatosAdicionales.AddRange(notaDtoList);
                    }
                    else
                        respBusiness.Errores = respDatos.Errores;
                }
            }
            else
                respBusiness.Errores.Add(Mensajes.NoFiltrosAplicados);

            return respBusiness;
        }

        #endregion "Filtrar"

        #region "Funciones y métodos auxiliares"

        /// <summary>
        /// Obtiene un objeto NotaDto a partir de un HttpRequestBase
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private NotaDto ObtenerNotaRequest(HttpRequestBase request, BusinessResponseDto respBusiness)
        {
            NotaDto notaDto = new NotaDto();

            try
            {
                NameValueCollection RequestFormValidated = request.Form;

                notaDto.Titulo = RequestFormValidated["Titulo"];
                notaDto.Texto = RequestFormValidated["__Texto"];
                notaDto.Color = RequestFormValidated["Color"];
                notaDto.Anclada = Convert.ToBoolean(RequestFormValidated["Anclada"]);
                notaDto.NotaId = Convert.ToInt32(RequestFormValidated["NotaId"]);
                notaDto.FechaCreacion = Convert.ToDateTime(RequestFormValidated["FechaCreacion"]);
                notaDto.Activo = Convert.ToBoolean(RequestFormValidated["Activo"]);
            }
            catch (Exception ex)
            {
                respBusiness.Errores.Add(Mensajes.ErrorInesperado);
                log.Error(ex.ToString());
            }

            return notaDto;
        }

        #endregion "Funciones y métodos auxiliares"
    }

}
