﻿using Business.Dto;
using Data.Models;
using System.Collections.Generic;

namespace Business.Utils
{
    internal class Conversors
    {
        internal static Nota NotaDto_to_Nota(NotaDto notaDto)
        {
            var nota = new Nota {
                Activo = notaDto.Activo,
                Anclada = notaDto.Anclada,
                FechaCreacion = notaDto.FechaCreacion,
                FechaEliminacion = notaDto.FechaEliminacion,
                FechaModificacion = notaDto.FechaModificacion,
                NotaId = notaDto.NotaId,
                Texto = notaDto.Texto,
                Titulo = notaDto.Titulo, 
                Color = notaDto.Color
            };

            return nota;
        }

        internal static List<Nota> NotaDto_to_Nota(List<NotaDto> notaDtoList)
        {
            List<Nota> notaList = new List<Nota>();

            foreach (NotaDto notaDto in notaDtoList)
                notaList.Add(NotaDto_to_Nota(notaDto));

            return notaList;
        }
    }
}
