﻿using System;
using System.Linq;

namespace Business.Dto
{
    public class FiltroNotaDto
    {
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public DateTime? FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public bool? Anclada { get; set; }

        /// <summary>
        /// Indica si hay un filto aplicado o no
        /// </summary>
        public bool Existe
        {
            get
            {
                var filtroTitulo = this.Titulo != null && this.Titulo.Trim().Count() > 0;
                var filtroTexto = this.Texto != null && this.Texto.Trim().Count() > 0;
                var filtroFecha = this.FechaDesde != null && this.FechaHasta != null;
                var anclada = this.Anclada != null;

                return filtroTitulo || filtroTexto || filtroFecha || anclada;
            }
        }
    }
}
