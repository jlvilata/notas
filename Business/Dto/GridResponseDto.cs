﻿using System.Collections.Generic;

namespace Business.Dto
{
    public class GridResponseDto<T>
    {
        public int draw { get; set; }

        public long recordsTotal { get; set; }
        private long? _recordsFiltered { get; set; }

        public IEnumerable<T> data { get; set; }
    }
}
