﻿using Data.Models;
using System.Collections.Generic;

namespace Business.Dto
{
    public class NotaDto: Nota
    {

        #region "Constructores"

        public NotaDto()
        {
        }

        public NotaDto(Nota nota)
        {
            this.NotaId = nota.NotaId;
            this.Titulo = nota.Titulo;
            this.Texto = nota.Texto;
            this.Activo = nota.Activo;
            this.Anclada = nota.Anclada;
            this.FechaCreacion = nota.FechaCreacion;
            this.FechaEliminacion = nota.FechaEliminacion;
            this.Color = nota.Color;
        }

        #endregion "Constructores"

        #region "Conversores"

        internal static List<NotaDto> Nota_to_NotaDto(List<Nota> notaList)
        {
            List<NotaDto> notaDtoList = new List<NotaDto>();

            if (notaList != null)
            {
                foreach (Nota nota in notaList)
                    notaDtoList.Add(new NotaDto(nota));
            }

            return notaDtoList;
        }

        #endregion "Conversores"
    }
}
