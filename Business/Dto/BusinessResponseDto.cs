﻿using Data.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Business.Dto
{
    public class BusinessResponseDto
    {

        #region "Propiedades"

        public List<string> Errores { get; set; }

        public bool Ok
        {
            get
            {
                return !this.Errores.Any();
            }
        }

        #endregion "Propiedades"

        #region "Constructores"

        public BusinessResponseDto()
        {
            this.Errores = new List<string>();
        }

        public BusinessResponseDto(DataResponseDto dataResponseDto)
        {
            if (!dataResponseDto.Ok)
                this.Errores = dataResponseDto.Errores;
        }

        #endregion "Constructores"

    }

    public class BusinessResponseDto<T> : BusinessResponseDto
    {

        #region "Propiedades"

        public T DatosAdicionales { get; set; }

        #endregion "Propiedades"

        #region "Constructores"

        public BusinessResponseDto()
        {
            this.Errores = new List<string>();
        }

        public BusinessResponseDto(DataResponseDto<T> dataResponseDto)
        {
            if (dataResponseDto.Ok)
                this.DatosAdicionales = dataResponseDto.DatosAdicionales;
            else
                this.Errores = dataResponseDto.Errores;
        }

        #endregion "Constructores"
    }
}
