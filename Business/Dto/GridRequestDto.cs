﻿using System.Collections.Generic;

namespace Business.Dto
{
    public class GridRequestDto
    {
        /// <summary>
        /// Numero de veces que hemos consultado al servidor
        /// </summary>
        public int Draw { get; set; }

        /// <summary>
        /// Registro a partir del cual vamos a recuperar
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Numero de registros a recuperar
        /// </summary>
        public int Length { get; set; }

        public GridSearchDto Search { get; set; }

        public List<GridOrderDto> Order { get; set; }
        public List<GridColumnDto> Columns { get; set; }
    }

    public class GridSearchDto
    {
        public string Value { get; set; }
        public bool Regex { get; set; }
    }

    public class GridOrderDto
    {
        public int Column { get; set; }
        public string Dir { get; set; }
    }

    public class GridColumnDto
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }

        public GridSearchDto Search { get; set; }
    }
}
